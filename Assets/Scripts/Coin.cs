﻿using UnityEngine;
using UnityEngine.UI;

public class Coin : MonoBehaviour {

    public AudioClip coin;

	private void OnTriggerEnter2D(Collider2D col)
	{
		if (Goal.IsStop) return;
		if (col.tag == "Player")
		{
			GameController.AddOneCoin(transform.position);
            AudioSource.PlayClipAtPoint(coin,Camera.main.transform.position, 1);
			Destroy(gameObject);
		}
	}
}
