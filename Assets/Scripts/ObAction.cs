﻿using UnityEngine;

public class ObAction : MonoBehaviour {	
	public Transform target;
	public StyleAction actionIndex = StyleAction.None;
	public float delayTime = 0;
	public float distanceFall = 5f;
	public float roSpeed = 10f;
	public float moSpeed = 1f;
	public Vector2 velocity;
	public bool isTouched = false;
	private StyleAction curIndex;
	private Rigidbody2D rb;
	private float time;
	private float scale;
	private SpriteRenderer sprite;
	// Use this for initialization
	void Start () {
		curIndex = StyleAction.None;
		rb = GetComponent<Rigidbody2D>();
		rb.gravityScale = 0;
		sprite = GetComponent<SpriteRenderer>();
		SetColor();
	}
	
	// Update is called once per frame
	void Update () {
		switch (actionIndex)
		{
			case StyleAction.Fall:
				action0();
				break;			
			default:
				break;
		}
	}
	void FixedUpdate()
	{
		switch (actionIndex)
		{
			case StyleAction.Rotate:
				action1();
				break;
			case StyleAction.Move:
				action2();
				break;
			case StyleAction.Shake:
				action3();
				break;
			default:
				break;
		}
	}
	public enum StyleAction
    {
		None,
        Fall,
        Rotate,
		Move,
		Shake
    };
	private void SetColor()
	{
		switch (actionIndex)
		{
			case StyleAction.Rotate:
				sprite.color = Color.red;
				break;
			case StyleAction.Move:
				sprite.color = Color.green;
				break;
			case StyleAction.Shake:
				sprite.color = Color.yellow;
				break;
			default:
				sprite.color = Color.gray;
				break;
		}
	}
	private void action0()
	{
		if (curIndex == actionIndex) return;
		if (Vector3.Distance(target.position,transform.position) < distanceFall)
		{
			curIndex = actionIndex;
			rb.gravityScale = 1f;
		}
	}
	private void action1()
	{
		rb.MoveRotation(rb.rotation + roSpeed * Time.fixedDeltaTime);
	}	
	private void action2()
	{
		if (!isTouched) return;
		time += Time.deltaTime;
		scale = moSpeed * Mathf.Cos(time) * 0.4f;
		rb.MovePosition(rb.position + velocity * scale * Time.deltaTime);	
	}
	private void action3()
	{
		if (!isTouched) return;
		time += Time.deltaTime;
		scale = roSpeed * Mathf.Cos(time);
		rb.MoveRotation(rb.rotation + scale);		
	}
	private void OnCollisionEnter2D()
	{
		if (isTouched) return;
		isTouched = true;
		time = 0;
		scale = 0;
	}
}
