using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class CarController : MonoBehaviour {
	public AudioClip clMotor1;
	public AudioClip clMotor2;
	public AudioClip clMotor3;
	private AudioSource audio;
	
	public float speed = 1500f;
	public float rotationSpeed = 15f;

    public WheelJoint2D backWheel;
    public WheelJoint2D frontWheel;
	
	public GameController gcontroller;
	
    private Rigidbody2D rb;
	private float movement = 0f;
	private float rotation = 0f;

    private bool IsRo;
	private float maxAngle = 240;
	
	public bool IsEnlessMode;
	public static bool isRound;
	private bool isDown = false;
    private void Awake()
    {
        if (SceneManager.GetActiveScene().name=="MainLevel")
        gcontroller = GameObject.Find("GameController").GetComponent<GameController>();
		Goal.IsStop = false;
		isRound = false;
        Physics2D.IgnoreLayerCollision(8, 9);
        rb = GetComponent<Rigidbody2D>();
		audio = GetComponent<AudioSource>();
    }
    void Update()
    {
		IsRo = transform.GetChild(0).GetComponent<Wheel>().InAir() && transform.GetChild(1).GetComponent<Wheel>().InAir();
		if (!IsRo)
		{
			rotation = 0;
			if (gcontroller) gcontroller.ResetRossocre();
		}
		if (Input.GetMouseButton(0) && !Goal.IsStop)
		{
			isDown = true;
			if (IsRo)
			{
				rotation += 0.5f;
                if (rotation >= rotationSpeed) rotation = rotationSpeed;
			}
			else
			{
				Sound_SpeedUp();
				movement -= 120f;				
				if (movement <= -speed) movement = -speed;
			}
		}
		else
		{
			Sound_SpeedDown();
			isDown = false;
			rotation = 0;
			movement = 0f; 
		}
    }

    void FixedUpdate()
    {
		if (isDown)
		{
			if (IsRo)
			{
				rb.MoveRotation(rb.rotation + rotation);
				UpdateRotation();
			}
			else
			{
				JointMotor2D motor = new JointMotor2D { motorSpeed = movement, maxMotorTorque = 10000 };
				backWheel.motor = motor;
				frontWheel.motor = motor;
			}
		}
		else
		{
			rb.angularVelocity /= 1.5f;
			backWheel.useMotor = false;
            frontWheel.useMotor = false;
		}
    }
	private void UpdateRotation()
	{
		if (rb.rotation > maxAngle) isRound = true;
		if (isRound && gcontroller)
		{
			gcontroller.AddRossocre();
			maxAngle += 360f;
			isRound = false;
		}
	}
	private void Sound_SpeedDown()
	{
		if (audio.clip != clMotor3) 
		{
			audio.loop = false;
			audio.clip = clMotor3;
			audio.Play();
		}
	}
	private void Sound_SpeedUp()
	{
		if (audio.clip == clMotor3) 
		{
			audio.loop = false;
			audio.clip = clMotor2;
			audio.Play();
		}
		if (audio.clip == clMotor2 && !audio.isPlaying)
		{
			audio.loop = true;
			audio.clip = clMotor1;
			audio.Play();
		}
	}

}
