﻿using UnityEngine;

public class TextRotation : MonoBehaviour {
	public Transform car;
	private bool Using = false;
	private MeshRenderer sp;
	private TextMesh text;
	private Vector3 offset;
	// Use this for initialization
	void Start () {
        car = CameraController.target;
		offset = transform.position - car.position;
		sp = GetComponent<MeshRenderer>();
		sp.enabled=Using;
		text = GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!Using || !car) return;
		transform.position = car.position+offset;
	}
	public void Show(int number)
	{
		GetComponent<AudioSource>().Play();
		transform.position = car.position+offset;
		Using = true;
		sp.enabled=Using;
		text.text = "+"+number.ToString();
		Invoke("Hide",1f);
	}
	private void Hide()
	{
		Using = false;
		sp.enabled=Using;
	}
}
