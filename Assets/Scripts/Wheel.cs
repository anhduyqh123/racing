﻿using UnityEngine;

public class Wheel : MonoBehaviour {
    ContactPoint2D[] contacts;
    private void Start()
    {
        contacts = new ContactPoint2D[1];
    }
    public bool InAir()
    {
        return GetComponent<Collider2D>().GetContacts(contacts) == 0;
    }
}
