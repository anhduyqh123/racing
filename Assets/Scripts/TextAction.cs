﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using UnityEngine.EventSystems;

public class TextAction : MonoBehaviour
{
    /*
    private Text text;
    public float speed = 1f;
    private Vector3 pos = new Vector3(0, 0, 30);
    // Use this for initialization
    void Start()
    {
        Goal.IsStop = true;
        text = GetComponent<Text>();
        text.text = SceneManager.GetActiveScene().name;
        Invoke("zoomText", 3f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, pos, speed);
    }
    private void zoomText()
    {
        pos = Vector3.zero;
        Invoke("hideText", 0.5f);
    }
    private void hideText()
    {
        text.enabled = false;
        Goal.IsStop = false;
    }*/

    private Text text;
    public GameObject btnHideLevelName;
    void Start()
    {
        Goal.IsStop = true;
        text = GetComponent<Text>();
        text.text = SceneManager.GetActiveScene().name.Insert(5, " ");
        Invoke("HideLevelName", 2f);
        
    }
    void HideLevelName()
    {
        gameObject.GetComponent<DoozyUI.UIElement>().Hide(false);
        Goal.IsStop = false;
    }
}
