﻿using UnityEngine;
using UnityEngine.UI;

public class Guide : MonoBehaviour {
	 
	 public Sprite touchedHand;
	 public Sprite normalHand;
	 public SpriteRenderer hand;
	 public Text textGuide;
	 public GameObject player;
	 public GameObject endPanel;	
	 public Transform tranText;
	 
	private int index;
	private Rigidbody2D rb;
	private Color color = new Color(1f,1f,1f,0f);
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectsWithTag("Player")[1];
		rb = player.GetComponent<Rigidbody2D>();
		SetAction(0);
		InvokeRepeating("SwapHand", 0, 0.1f);
	}
	
	// Update is called once per frame
	void Update () {
		CheckEnd();
		color.a += 0.01f;
		textGuide.color = color;
	}
	private void CheckEnd()
	{
		switch (index)
		{
			case 0:
				if (player.transform.position.x > 7f ) SetAction(index+1);
				break;
			case 1:
				if (CarController.isRound)
				{
					rb.bodyType = RigidbodyType2D.Dynamic;
					SetAction(index+1);
				}
				break;
			case 2:
				if (player.transform.position.x > 13f ) SetAction(index+1);
				break;
			default:
				break;
		}
	}
	private void SetAction(int _index)
	{
		index = _index;
		switch (index)
		{
			case 0:
				hand.sprite = normalHand;
				Invoke("SetTouchHand",1f);
				SetText("press and hold to accelerate...");
				break;
			case 1:
				hand.sprite = normalHand;
				Invoke("SetTouchHand",1f);
				rb.bodyType = RigidbodyType2D.Kinematic;
				rb.velocity = Vector2.zero;
				SetText("when the car isn't in ground, press and hold to flip...");
				Goal.IsStop = true;
				break;
			case 2:
				SetText("press continuity to reduce speed...");
				break;
			case 3:
				SetText("Now! Try to go goal!");
				hand.enabled = false;
				break;
			default:
				break;
		}
	}
	private void OnTriggerEnter2D()
	{
		Goal.IsStop = true;
		SetText("Good Job!");
		PlayerPrefs.SetInt("guide",1);
		Invoke("ShowEndPanel",1f);
	}
	private void ShowEndPanel()
	{
		endPanel.SetActive(true);
	}
	private void SetTouchHand()
	{
		hand.sprite = touchedHand;
		Goal.IsStop = false;
	}
	private void SwapHand()
	{
		if (index == 2)
		{
			if (hand.sprite == touchedHand) hand.sprite = normalHand;
			else hand.sprite = touchedHand;
		}
	}
	private void SetText(string s)
	{
		color.a = 0f;
		textGuide.color = color;
		textGuide.text = s;
	}
}
