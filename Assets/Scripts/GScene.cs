﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GScene : MonoBehaviour {
    private static bool IsSound = true;
    public GameObject PanelPause;
    public GameObject Loading;

    public GameObject panelSettings;
    public GameObject panelQuit;
	public Text coin;
    private void Start()
    {
		if (coin) coin.text = PlayerPrefs.GetInt("coin", 0).ToString();
        Time.timeScale = 1;
    }
    public static void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
    public void LoadGame()
    {
        if (PlayerPrefs.GetInt("guide",0) == 0) LoadScene("Guide");
		else
        StartCoroutine(LoadAsynchronously());
    }
    private IEnumerator LoadAsynchronously()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync("MainLevel");
        Image fillLoading = Loading.transform.GetChild(0).GetChild(0).GetComponent<Image>();
        Text textLoading = Loading.transform.GetChild(0).GetChild(1).GetComponent<Text>();
        Loading.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            fillLoading.fillAmount = progress;
            textLoading.text = ((int)progress * 100).ToString() + "%";
            yield return null;
        }
    }
	public void LoadThisScene()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
	public void LoadNextScene()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}
    public void LoadScene_name(string scene)
    {
        SceneManager.LoadScene(scene);
    }
    public void TurnSound()
    {
        IsSound = !IsSound;
        if (IsSound) AudioListener.volume = 1;
        else AudioListener.volume = 0;
    }
    public void PauseGame()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            PanelPause.GetComponent<DoozyUI.UIElement>().Show(false);
        }
        else
        {
            Time.timeScale = 1;
            PanelPause.GetComponent<DoozyUI.UIElement>().Hide(false);
        }
    }
    public void ResetTimeScale()
    {
        if (Time.timeScale==0)
        Time.timeScale = 1;
    }
    public void QuitGame()
    {
        Application.Quit();
    }

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			int index = SceneManager.GetActiveScene().buildIndex;
			switch (index)
			{
				case 0:
                    if (panelSettings.GetComponent<Canvas>().enabled == true)
                        panelSettings.GetComponent<DoozyUI.UIElement>().Hide(false);
                    else if (panelQuit.GetComponent<Canvas>().enabled == true)
                        panelQuit.GetComponent<DoozyUI.UIElement>().Hide(false);
                    else
                        panelQuit.GetComponent<DoozyUI.UIElement>().Show(false);
					break;
				case 2:
					LoadScene("Menu");
					break;
				case 3:
					LoadScene("Menu");
					break;
				case 4:
					LoadScene("Menu");
					break;
				default :
					PauseGame();
					break;					
			}
		}
	}

    public void NextPanelLevel()
    {
        GameObject levelArr = GameObject.FindGameObjectWithTag("PanelLevel");
        for (int i = 0; i < levelArr.transform.GetChildCount(); i++)
        {
            if (levelArr.transform.GetChild(i).gameObject.active == true)
            {
                levelArr.transform.GetChild(i).gameObject.SetActive(false);
                levelArr.transform.GetChild(i + 1).gameObject.SetActive(true);
                return;
            }
        }
    }

    public void BackPanelLevel()
    {
        GameObject levelArr = GameObject.FindGameObjectWithTag("PanelLevel");
        for (int i = 0; i < levelArr.transform.GetChildCount(); i++)
        {
            if (levelArr.transform.GetChild(i).gameObject.active == true)
            {
                levelArr.transform.GetChild(i).gameObject.SetActive(false);
                levelArr.transform.GetChild(i - 1).gameObject.SetActive(true);
                return;
            }
        }
    }

    public void LoadLevelAfter(string level)
    {
        StartCoroutine(LoadLevelAfterDelay(level));
    }
    IEnumerator LoadLevelAfterDelay(string level)
    {
        yield return new WaitForSeconds(1);
        Application.LoadLevel(level);
    }
}