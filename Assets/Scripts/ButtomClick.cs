﻿using UnityEngine;
using UnityEngine.UI;

public class ButtomClick : MonoBehaviour {
    private Button button;
    private Image image;
    public int price;
    private bool IsBought;
    // Use this for initialization
    void Start()
    {
        button = GetComponent<Button>();
        image = GetComponent<Image>();
        transform.GetChild(0).GetComponent<Text>().text = "$" + price.ToString();
        if (PlayerPrefs.GetInt(name, 0) == 0) IsBought = false;
        else Bought();
        if (PlayerPrefs.GetInt("car", 0) != transform.GetSiblingIndex() && IsBought) image.color = new Color(1f, 1f, 1f, 0.5f);
        button.onClick.AddListener(EvenClick);
    }
    private void Bought()
    {
        IsBought = true;
        image.sprite = ShopCars.GetSpriteCar(transform.GetSiblingIndex());
        transform.GetChild(0).gameObject.SetActive(false);
    }
    void EvenClick()
    {
        if (!IsBought)
        {            
            int coin = PlayerPrefs.GetInt("coin");
            if (coin < price) return;
            PlayerPrefs.SetInt("coin", coin - price);
            GameObject.Find("ValueCoin").GetComponent<Text>().text = PlayerPrefs.GetInt("coin", 0).ToString();
            PlayerPrefs.SetInt(name, 1);
            Bought();
        }
        transform.parent.GetChild(PlayerPrefs.GetInt("car", 0)).GetComponent<Image>().color = new Color(1f, 1f, 1f, 0.5f);
        image.color = new Color(1f, 1f, 1f, 1f);
        PlayerPrefs.SetInt("car", transform.GetSiblingIndex());
    }
}
