﻿using UnityEngine;
using UnityEngine.UI;

public class ShopCars : MonoBehaviour
{
    public Sprite[] local_listCars;
    public GameObject[] local_obCars;
    public static Sprite[] listCars;
    public static GameObject[] obCars;
    // Use this for initialization
    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("ShopCar");
        if (objs.Length > 1)
            Destroy(this.gameObject);
        DontDestroyOnLoad(this.gameObject);
        listCars = local_listCars;
        obCars = local_obCars;
        DontDestroyOnLoad(this.gameObject);
        //int index = PlayerPrefs.GetInt("car", 0);
        //curCar = listCars[index];
        //GameObject.Find("vlCoin").GetComponent<Text>().text = PlayerPrefs.GetInt("coin", 0).ToString();
    }
    public static Sprite GetSpriteCar(int index)
    {
        return listCars[index];
    }
    public static GameObject GetGObjectCar(int index)
    {
        return obCars[index];
    }
}

