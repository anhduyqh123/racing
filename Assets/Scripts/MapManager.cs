﻿using UnityEngine;

public class MapManager : MonoBehaviour {
    public GameObject[] listMaps;
    private Transform player;
    private int x = 0;
    private GameObject CurMap, AfterMap;
	// Use this for initialization
	void Start () {
		player = CameraController.target;
        CurMap = Instantiate(listMaps[Random.Range(0, listMaps.Length)],Vector3.left,Quaternion.identity);
        AfterMap = CurMap;
	}
	
	// Update is called once per frame
	void Update () {
        CreateNewMap();
	}
    private void CreateNewMap()
    {
		if (!player) return;
        if (player.position.x>x)
        {
            x += 30;
            if (x>30) Destroy(CurMap, 3f);
            CurMap = AfterMap;
            AfterMap = Instantiate(listMaps[Random.Range(0, listMaps.Length)],Vector3.right*x,Quaternion.identity);
        }
    }
}
