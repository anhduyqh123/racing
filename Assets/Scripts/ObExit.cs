﻿using UnityEngine;

public class ObExit : MonoBehaviour {
    public float time = 1f;
	
	void Start()
	{
		GetComponent<SpriteRenderer>().color = Color.blue;
	}
	
    private void OnCollisionExit2D(Collision2D collision)
    {
        Destroy(gameObject, time);
    }
}
