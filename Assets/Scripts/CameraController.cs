using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class CameraController : MonoBehaviour {

	public static Transform target;

	public float smoothSpeed = 0.125f;
	private Vector3 offset = new Vector3(0,0,-10);
	void Awake()
	{		
        GameObject ob = GameObject.Find("Car");
        //d2
        Vector3 carPos = new Vector3();
        if (ob)
        {
            carPos = ob.transform.position;
            Destroy(ob);
        }
        target = Instantiate(ShopCars.GetGObjectCar(PlayerPrefs.GetInt("car", 0)), carPos, Quaternion.identity).transform;
		//target = Instantiate(ShopCars.GetGObjectCar(8), new Vector3(0,3,0), Quaternion.identity).transform;
	}
	void FixedUpdate ()
	{
		if (!target) return;
		offset = new Vector3(0,0,-10 - Mathf.Abs(target.position.y));
		Vector3 desiredPosition = target.position + offset;
		//Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
		Vector3 smoothedPosition = Vector3.MoveTowards(transform.position, desiredPosition, Vector3.Distance(transform.position, target.position)/52f);
		transform.position = smoothedPosition;
	}

}
