﻿using UnityEngine;
using UnityEngine.UI;

public class PanelLevel : MonoBehaviour {

	public Sprite[] Star;
	
	// Use this for initialization
	void Start () {
		UpdatePanelLevel();
	}
	void UpdatePanelLevel()
    {
        GameObject levelArr = GameObject.FindGameObjectWithTag("PanelLevel");
        string levelStar = PlayerPrefs.GetString("LevelStar", "0000000000000000000000000000000000000000");
        char[] charArr = levelStar.ToCharArray();

        int i = 0;
        
        for (int j = 0; j < levelArr.transform.GetChildCount() - 2; j++)
        {
            for (int k = 3; k < levelArr.transform.GetChild(j).GetChildCount(); k++)
            {
                levelArr.transform.GetChild(j).GetChild(k).GetChild(1).gameObject.GetComponent<Text>().text = "Level " + (i+1).ToString();
                for (int l = 0; l < charArr[i] - '0'; l++)
                {
                    levelArr.transform.GetChild(j).GetChild(k).GetChild(2).gameObject.transform.GetChild(l).gameObject.GetComponent<Image>().sprite = Star[1];
                }
                string level = "level" + (i + 1).ToString();
                levelArr.transform.GetChild(j).GetChild(k).GetChild(3).gameObject.GetComponent<Text>().text = Goal.FormatTime(PlayerPrefs.GetInt(level));
                i++;
            }
        }
  
    }
}
