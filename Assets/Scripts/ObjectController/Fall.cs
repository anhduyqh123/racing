﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fall : MonoBehaviour {
    private bool IsUsed = false;
    public float time;
    private void Start()
    {
        GetComponent<SpriteRenderer>().material.color = Color.blue;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (IsUsed) return;
        if (collision.gameObject.tag != "Wheel") return;
        IsUsed = true;
        Invoke("setRigibody", time);
        Destroy(gameObject, time+1f);
    }
    private void setRigibody()
    {
        gameObject.AddComponent<Rigidbody2D>();
    }
}
