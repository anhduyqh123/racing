﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {
    
    public float time;
    Rigidbody2D rb2D;
    public bool isEnd = false;
    public bool isMove = false;
    Vector3 dir;
    public Vector3 targetWayPoint;
    public float speed;
    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        dir = (targetWayPoint - transform.position);
		GetComponent<SpriteRenderer>().material.color = Color.blue;
    }
    private void Update()
    {
        if (isEnd) return;
        if (Vector3.Distance(rb2D.position,targetWayPoint)<0.5)
            isEnd =true;
        if (isMove)
            rb2D.MovePosition(new Vector3(rb2D.position.x,rb2D.position.y) + dir*Time.deltaTime*speed);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wheel")
            Invoke("SetIsMove", time);
    }
    
    void SetIsMove()
    {
        isMove = true;
    }
    
}
