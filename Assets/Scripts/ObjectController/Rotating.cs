﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotating : MonoBehaviour {
    Rigidbody2D rb2D;
    public float speed;
    public bool isRotating;
    public bool CheckFalseRotate;
    public bool isRotateWhenCollision;
    // Use this for initialization
    void Start () {
        rb2D = GetComponent<Rigidbody2D>();
        GetComponent<SpriteRenderer>().material.color = Color.blue;
    }
	
	// Update is called once per frame
	void Update () {
        if (isRotating)
        rb2D.MoveRotation(rb2D.rotation + speed * Time.fixedDeltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wheel" && (CheckFalseRotate))
        {
            if (isRotating)
                isRotating = false;
        }
        if (collision.gameObject.tag == "Wheel" && (isRotateWhenCollision))
        {
            if (!isRotating)
            {
                isRotating = true;
            }
        }

    }
}
