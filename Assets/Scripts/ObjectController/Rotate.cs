﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{

    private bool isRotate = false;
    public float speed;
    public float time;
    Rigidbody2D rb2D;
    public float rotation;
    public float curRotation;
    public bool isEnd = false;
    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        curRotation = rb2D.rotation;
		GetComponent<SpriteRenderer>().material.color = Color.blue;
    }
    private void Update()
    {
        if (isEnd) return;
        if (rb2D.rotation != curRotation && ((rotation > 0 && rb2D.rotation > rotation) || (rotation < 0 && rb2D.rotation < rotation)))
            isEnd = true;
        if (isRotate)
        if (rotation>0)
            rb2D.MoveRotation(rb2D.rotation + speed * Time.fixedDeltaTime);
        else 
            if (rotation<0)
            rb2D.MoveRotation(rb2D.rotation - speed * Time.fixedDeltaTime);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wheel")
            Invoke("SetIsRotate", time);
    }
    void SetIsRotate()
    {
        isRotate = true;
    }
}
