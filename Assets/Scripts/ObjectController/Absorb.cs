﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Absorb : MonoBehaviour {
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wheel")
        {
            GameObject[] wheels;
            wheels = GameObject.FindGameObjectsWithTag("Wheel");
            for (int i = 0; i < wheels.Length; i++)
                if (wheels[i].GetComponent<Wheel>().InAir() == true)
                    return;
            collision.transform.parent.GetComponent<Rigidbody2D>().gravityScale = 0;
            collision.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector3.right * 100f);
        }
            
    }

}
