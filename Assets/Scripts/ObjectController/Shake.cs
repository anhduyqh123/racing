﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour {

    Rigidbody2D rb2D;
    public float rotation;
    public float speed;
    public bool isShaking;
    public bool CheckFalseShake;
    public bool isShakeWhenCollision;
    // Use this for initialization
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        GetComponent<SpriteRenderer>().material.color = Color.blue;
    }

    // Update is called once per frame
    void Update()
    {
        if (isShaking)
        {
            if (Mathf.Abs(rb2D.rotation) > rotation)
                speed = -speed;
            rb2D.MoveRotation(rb2D.rotation + speed * Time.deltaTime);
        }
            
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wheel" && (CheckFalseShake))
        {
            if (isShaking)
                isShaking = false;
        }
        if (collision.gameObject.tag == "Wheel" && (isShakeWhenCollision))
        {
            if (!isShaking)
            {
                isShaking = true;
            }
        }

    }
}
