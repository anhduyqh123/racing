﻿using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	public GameObject obText;
    public Text scoreText;
    private Transform player;
	public TextRotation textRo;
	
    public int totalScore = 0;
    private int Disscore = 0;
    private int Roscore = 0;
	
	private static int coin;
	private static Text txtCoin;
	private static GameObject staticObText;

	// Use this for initialization
	void Start () {
		player = CameraController.target;
        scoreText.text = totalScore.ToString();
		coin = PlayerPrefs.GetInt("coin",0);
		txtCoin = GameObject.Find("ValueCoin").GetComponent<Text>();
		txtCoin.text = coin.ToString();
		staticObText = obText;
	}
	
	// Update is called once per frame
	void Update () {
		if (Goal.IsStop) return;
        Disscore = (int)player.position.x;
		if (Disscore + Roscore>totalScore)
        {
            totalScore = Disscore + Roscore;
            scoreText.text = totalScore.ToString();
        }       
	}
    public void AddRossocre()
    {
        Roscore++;
		textRo.Show(Roscore);
    }
	public void ResetRossocre()
	{
		Roscore=0;
	}
	public static void AddOneCoin(Vector3 pos)
	{
		Destroy(Instantiate(staticObText, pos, Quaternion.identity),1f);
		coin++;
		PlayerPrefs.SetInt("coin",coin);
		txtCoin.text = coin.ToString();
	}
}
