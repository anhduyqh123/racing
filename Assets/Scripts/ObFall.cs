﻿using UnityEngine;

public class ObFall : MonoBehaviour {
    private bool IsUsed = false;
	public float time = 0.2f;
	void Start()
	{
		GetComponent<SpriteRenderer>().color = Color.blue;
	}
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (IsUsed) return;
        IsUsed = true;
        Invoke("setRigibody", time);
		Destroy(gameObject, 1f);
    }
    private void setRigibody()
    {
        gameObject.AddComponent<Rigidbody2D>();
    }
}
