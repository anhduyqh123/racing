using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class EndGame : MonoBehaviour {
	public GameObject boomb;
    private GameObject panelWinGame;
    private void Awake()
    {
        panelWinGame = GameObject.Find("PanelWingame");
    }
    private void Start()
    {
        Time.timeScale = 1;
    }
    void Update()
	{
		if (Goal.IsStop) return;
        if (transform.position.y < -20f)
		{
			Goal.IsStop = true;
			GameOver(true);
		}
	}
	void OnTriggerStay2D (Collider2D colInfo)
	{
		if (Goal.IsStop || colInfo.tag == "Finish") return;
		if (transform.eulerAngles.z < 240 && transform.eulerAngles.z> 145)
		{
			Goal.IsStop = true;
			Instantiate(boomb, transform.position + Vector3.back, Quaternion.identity);
            GameOver();
        }
    }
	private void OnCollisionEnter2D(Collision2D col)
	{
		if (Goal.IsStop) return;
		if (col.collider.tag == "Trap")
		{
			Goal.IsStop = true;
			Invoke("LoadSceneAgain",1f);
		}
	}
	private void LoadSceneAgain()
	{
		GScene.LoadScene(SceneManager.GetActiveScene().name);
	}
    private void GameOver(bool fall = false)
    {
        if (SceneManager.GetActiveScene().name == "MainLevel")
        {
            PlayerPrefs.SetInt("score", GameObject.Find("GameController").GetComponent<GameController>().totalScore);
            if (PlayerPrefs.GetInt("highscore") < PlayerPrefs.GetInt("score"))
                PlayerPrefs.SetInt("highscore", PlayerPrefs.GetInt("score"));			
            panelWinGame.transform.GetChild(2).GetChild(0).gameObject.GetComponent<Text>().text = PlayerPrefs.GetInt("score").ToString();
            panelWinGame.transform.GetChild(3).GetChild(0).gameObject.GetComponent<Text>().text = PlayerPrefs.GetInt("highscore").ToString();
            panelWinGame.GetComponent<DoozyUI.UIElement>().Show(false);
			panelWinGame.GetComponent<AudioSource>().Play();
        }
        else
		Invoke("LoadSceneAgain",1f);
		if (fall)
		{
			GetComponent<SpriteRenderer>().enabled = false;
			transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
			transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
		}
		else
		{
			GetComponent<SpriteRenderer>().enabled = false;
			GetComponent<CarController>().backWheel.enabled = false;
			GetComponent<CarController>().frontWheel.enabled = false;
		}
    }
       
}