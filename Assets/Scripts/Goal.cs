﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Goal : MonoBehaviour {

    public Sprite[] Star;

    public Text time;
	public Text best;
	public GameObject panelWinGame;
	
	public Text levelName;
	public Text Wtime;
	public Text Wbest;
	private float t;
	public static bool IsStop = false;
    
    public float t1;

    public static int[] arrayStar=new int[40];

	void Start()
	{
		levelName.text = SceneManager.GetActiveScene().name.Insert(5, " ");
        best.text = FormatTime(PlayerPrefs.GetInt(levelName.text,0));
	}
	void Update()
	{
		if (IsStop) return;
		t += Time.deltaTime;
		time.text = FormatTime(t);
	}
	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player") WinGame();
	}
	private void WinGame()
	{
		GetComponent<AudioSource>().Play();
		int bTime = PlayerPrefs.GetInt(levelName.text,0);

        CheckStar();

        if (bTime == 0 || t < bTime) PlayerPrefs.SetInt(levelName.text,(int)t);
        //winGame.SetActive(true);


        panelWinGame.GetComponent<DoozyUI.UIElement>().Show(false);

        Wtime.text = time.text;
		Wbest.text = FormatTime(PlayerPrefs.GetInt(levelName.text,0));
		IsStop=true;

        updateStar();
	}
	public static string FormatTime(float _t)
	{
		int _time = (int)_t;
		return (_time/60).ToString("00")+":"+(_time%60).ToString("00");
	}
    void CheckStar()
    {
        float bestTime = PlayerPrefs.GetInt(levelName.text);
        if ((int)t >= (int)bestTime && bestTime!=0)
            return;
        int n = int.Parse(levelName.text.Substring(5));
        string levelStar = PlayerPrefs.GetString("LevelStar", "0000000000000000000000000000000000000000");
        char[] charArr = levelStar.ToCharArray();
        if ((int)t <= (int)t1)
            charArr[n-1] = '3';
        else
            if ((int)t <= (int)(t1 + 6))
            charArr[n-1] = '2';
        else
            charArr[n-1] = '1';
        levelStar = new string(charArr);
        PlayerPrefs.SetString("LevelStar", levelStar);
    }

    void updateStar()
    {
        int index;
        if ((int)t <= (int)t1)
            index = 3;
        else
            if ((int)t <= (int)(t1 + 6))
            index = 2;
        else
            index = 1;
        GameObject starArr = GameObject.FindGameObjectWithTag("StarLine");
        
            updateSpriteStar(starArr, index);
        
    }

    void updateSpriteStar(GameObject go, int n)
    {
        for (int i = 0; i < n; i++)
        {
            go.transform.GetChild(i).gameObject.GetComponent<Image>().sprite = Star[1];
        }
    }
}
