﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UpdateScoreMenu : MonoBehaviour {
    public Text score;
    public Text highScore;
	// Use this for initialization
	void Start () {
        score.text = PlayerPrefs.GetInt("score",0).ToString();
        highScore.text = PlayerPrefs.GetInt("highscore", 0).ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
